const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Comment = sequelize.define('Comment', {
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    hour: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    likes_qnt: {
        type: DataTypes.NUMBER,

    },

    share_qnt: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

}, {

});

Comment.associate = function(models){
    Comment.belongsTo(models.Movie);
}

module.exports = Comment;