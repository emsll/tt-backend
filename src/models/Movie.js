const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Movie = sequelize.define("Movie", {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },

    genre: {
        type: DataTypes.STRING,
        allowNull: false
    },

    age: {
        type: DataTypes.STRING,
        allowNull: false
    },

    synopsis: {
        type: DataTypes.STRING,
        allowNull: false
    },

    release_year: {
        type: DataTypes.NUMBER,
        allowNull: true
    },

    duration: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

}, {

});

Movie.associate = function(models){
    Movie.hasMany(models.Comment, {});

}

module.exports = Movie;

