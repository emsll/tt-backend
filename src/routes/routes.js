const { Router } = require('express');
const MovieController = require('../controllers/MovieController');
const router = Router();

router.get('/movies',MovieController.index);
router.get('/movies/:id',MovieController.show);
router.post('/movies',MovieController.create);
router.put('/movies/:id', MovieController.update);
router.delete('/movies/:id', MovieController.destroy);


const CommentController = require('../controllers/CommentController');

router.get('/comments',CommentController.index);
router.get('/comments/:id',CommentController.show);
router.post('/comments',CommentController.create);
router.get('/comments/:id',CommentController.show);
router.put('/comments/:id', CommentController.update);
router.put('/commentsaddmovies/:id', CommentController.addRelationMovie);
router.delete('/commentsremoverole/:id', CommentController.removeRelationMovie);
router.delete('/comments/:id', CommentController.destroy);


module.exports = router;




