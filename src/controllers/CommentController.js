const { response } = require('express');
const Comment = require('../models/Comment');
const { upsert } = require('../models/Movie');
const Movie = require('../models/Movie');

const create = async(req,res) => {
    const {id} = req.params;
    try{
        const comment = await Comment.create(req.body);
        return res.status(201).json({message: "Comentário criado com sucesso!", comment: comment});
    }catch(err){
        res.status(500).json({error: err});
    }    

};

const index = async(req,res) => {
    try{
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }

};

const show = async(req,res) => {
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }

};   

const update = async(req,res) => {
    const {id} = req.params;
    try{
        const [update] = await Comment.update(req.body, {where: {id: id}});
        if(update) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(movie);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encontrado.");
    }

};

const destroy = async(req,res) => {
    const {id} = req.params;
    try{
        const deleted = await Comment.destroy({where: {id: id}});
        if (deleted) {
            return res.status(200).json("Comentário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err) {
        return res.status(500).json("Comentário não encontrado.");
    }

};

const addRelationMovie = async(req,res) => {  
    try {  
        const comment = await Comment.findByPk(id);
        const movie = await Movie.findByPk(req.body.MovieId);
        await upsert.setMovie(movie);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }

};

const removeRelationMovie = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setMovie(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
    
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationMovie,
    removeRelationMovie

};